import spacy
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB
from translate import Translator
from langdetect import detect
import random


class HybridChatbot:
    def __init__(self, patterns, tags, responses):
        self.nlp = spacy.load("en_core_web_sm")
        self.vectorizer = TfidfVectorizer()
        self.model = MultinomialNB()
        X_train = self.vectorizer.fit_transform(patterns)
        self.model.fit(X_train, tags)
        self.responses = responses
        self.contexts = {}  # Armazenar contexto da conversa

    def update_context(self, user_id, text):
        if user_id not in self.contexts:
            self.contexts[user_id] = []
        self.contexts[user_id].append(text)
        self.contexts[user_id] = self.contexts[user_id][-5:]  # Mantém as últimas 5 mensagens

    def translate_text(self, text, dest_lang, src_lang='en'):
        if text is None:
            return ''

        if dest_lang not in ['en', 'es', 'fr', 'de', 'it', 'pt', 'ru', 'ja', 'zh-CN', 'ar', 'tr', 'nl']:
            dest_lang = 'en'
        if dest_lang == src_lang:
            return text
        translator = Translator(from_lang=src_lang, to_lang=dest_lang)
        return translator.translate(text)

    def detect_language(self, text):
        try:
            return detect(text)
        except:
            return 'en'

    def generate_local_response(self, user_input):
        X_input = self.vectorizer.transform([user_input])
        predicted_tag = self.model.predict(X_input)[0]
        prediction_proba = max(self.model.predict_proba(X_input)[0])

        if predicted_tag in self.responses:
            return random.choice(self.responses[predicted_tag]), prediction_proba
        else:
            return "Desculpe, não consegui entender sua pergunta.", 0

    def update_context(self, user_id, text):
        if user_id not in self.contexts:
            self.contexts[user_id] = {"messages": [], "language": None}
        self.contexts[user_id]["messages"].append(text)
        # Limitar histórico de mensagens para evitar uso excessivo de memória
        self.contexts[user_id]["messages"] = self.contexts[user_id]["messages"][-5:]

    def generate_response(self, user_input, user_id):
        self.update_context(user_id, user_input)

        # Detecta o idioma
        user_lang = self.detect_language(user_input)

        # Traduz
        if user_lang != 'en':
            translated_input = self.translate_text(user_input, 'en', src_lang=user_lang)
        else:
            translated_input = user_input

        # Gerar a resposta
        response, _ = self.generate_local_response(translated_input)

        # Traduzir a resposta
        if user_lang != 'en':
            translated_response = self.translate_text(response, user_lang, src_lang='en')
        else:
            translated_response = response

        return translated_response

    def set_user_language(self, lang_code):
        self.current_user_lang = lang_code
