import json
import nltk
from nltk.stem import WordNetLemmatizer


class EnhancedChatbotDataLoader:
    def __init__(self, file_path):
        self.file_path = file_path
        self.lemmatizer = WordNetLemmatizer()

    def load_intents(self):
        with open(self.file_path) as file:
            return json.load(file)

    def tokenize_and_lemmatize(self, text):
        # Baixar NLTK:
        # nltk.download('punkt')
        # nltk.download('wordnet')

        tokens = nltk.word_tokenize(text)
        lemmas = [self.lemmatizer.lemmatize(token.lower()) for token in tokens]
        return lemmas

    def prepare_data(self, intents):
        tags = []
        patterns = []
        responses = {}

        for intent in intents["intents"]:
            responses[intent["tag"]] = intent["responses"]
            for pattern in intent["patterns"]:
                lemmatized_pattern = self.tokenize_and_lemmatize(pattern)
                patterns.append(' '.join(lemmatized_pattern))
                tags.append(intent["tag"])

        return patterns, tags, responses
