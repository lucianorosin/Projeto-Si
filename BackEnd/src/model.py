from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.naive_bayes import MultinomialNB


def train_model(patterns, tags):
    vectorizer = TfidfVectorizer()
    X_train = vectorizer.fit_transform(patterns)
    model = MultinomialNB()
    model.fit(X_train, tags)
    return model, vectorizer
