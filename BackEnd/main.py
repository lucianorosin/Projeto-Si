from flask import Flask, request, jsonify
from flask_cors import CORS
from src.chatbot import HybridChatbot
from src.data_loader import EnhancedChatbotDataLoader

app = Flask(__name__)
CORS(app)

# Carregar e preparar dados
data_loader = EnhancedChatbotDataLoader('data/intents.json')
intents = data_loader.load_intents()
patterns, tags, responses = data_loader.prepare_data(intents)

chatbot = HybridChatbot(patterns, tags, responses)


@app.route('/chat', methods=['POST'])
def chat():
    data = request.json
    user_input = data.get("text")
    user_id = data.get("sender", "default_user")

    response = chatbot.generate_response(user_input, user_id)
    return jsonify({"text": response})


if __name__ == '__main__':
    app.run(debug=True)
